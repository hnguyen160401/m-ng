// dom tới thẻ tr
var trList = document.querySelectorAll("#tblBody tr");

function getScoreFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");

  //   lấy điểm ở cột td
  return tdList[3].innerText * 1;
}

var maxTr = trList[0];
for (let index = 0; index < trList.length; index++) {
  var currentTr = trList[index];

  var currentTrScore = getScoreFromTr(currentTr);
  console.log(currentTrScore);
  var maxScoreTr = getScoreFromTr(maxTr);
  if (currentTrScore > maxScoreTr) {
    maxTr = currentTr;
  }
}
console.log(maxScoreTr);
