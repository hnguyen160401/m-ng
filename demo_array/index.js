// primitive value ~~ pass by value ~ string, number, boolean

// reference value ~~ pass by reference ~~ object , array

var arraySdt = ["123", "456", "789"];
// console.log("arraySdt", arraySdt);
// console.log(arraySdt[2]);

var menu = ["bún bò", "chả xiên", "banh mỳ"];

//length là số lượng phần tử
var tongMonAn = menu.length;
console.log("tongMonAn", tongMonAn);

//update giá trị của 1 phần tử dựa vào vị trí index
menu[0] = "Bún bò huế";
console.log("menu", menu);

// duyệt mảng xuôi
for (var index = 0; index < menu.length; index++) {
  console.log("item", menu[index]);
}

// duyệt mảng ngược
for (var index = menu.length - 1; index >= 0; index--) {
  console.log("item", menu[index]);
}

menu.forEach(function (item, index) {
  console.log("item forEach", item, index);
});

// thêm item vào cuối
menu.push("cơm tấm");
menu.push("cơm chiên");
console.log("menu", menu);

// xoá item cuối cùng
menu.pop;
console.log("menu", menu);

// thêm item vào đầu
menu.unshift("cơm đùi gà");
console.log("menu", menu);

// xoá item ở đầu
menu.shift();
console.log("menu", menu);

var viTriComTam = menu.indexOf("cơm tấm");
console.log("viTriComTam", viTriComTam);

menu.splice(viTriComTam, 1);
console.log("viTriComTam", viTriComTam);

// // thay đổi cơm tấm
// console.log("menu", menu);
// menu[viTriComTam] = "cơm trắng";
// console.log("menu", menu);

// slice(vị trí bắt đầu, vị trí kết thúc) :copy các phần tử từ array sang array mới
var lettes = ["a", "b", "c", "d"];
var coppyArr = lettes.slice(0, 2);
console.log(coppyArr);
console.log(lettes);

// splice(vị trí bắt đầu, số lượng): cut các phần tử từ array sang array mới
var cutArray = lettes.splice(2, 1);
console.log(cutArray);
console.log(lettes);
